﻿using System.Collections.Generic;

namespace UrtPanel.Messages
{
    /// <summary>
    /// Helper objects that helps by groupying your entires so you don't overflow the maximal size of 1 disocrd message. 
    /// It doesn't handle if you provide a message that is bigger then the limit itself, but helps you by prematurly splitting the message based on your addition.
    /// </summary>
    public class DiscordMessage
    {
        public int MaxCharPerSegment = 1000; //1024

        string currString = "";
        List<string> stringBuffer = new List<string>();
        public void Add(string s)
        {
            if (currString.Length + s.Length > MaxCharPerSegment)
            {
                stringBuffer.Add(currString);
                currString = s;
            }
            else
                currString += s;
        }

        public List<string> Get()
        {
            if (!string.IsNullOrWhiteSpace(currString))
            {
                stringBuffer.Add(currString);
            }
            return stringBuffer;
        }
    }
}