﻿using Discord;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UrtPanel.Config;
using UrtPanel.Models;
using UrtPanel.Panel;
using UrtPanel.Thumbnails;
using UrTPanel.Messages;

namespace UrtPanel.Messages
{
    /// <summary>
    /// Constructs messages. 
    ///     - Game server Panel
    ///     - 
    /// </summary>
    public class MessageConstructor
    {
        MainConfig cfg;
        ThumbnailManager mapManager;

        InformativeTeamServerDecor informativeTeamServerDecor;
        CompactTeamServerDecor compactTeamServerDecor;
        PublicServerDecor publicServerDecor;
        CvarTableDecor cvarTableDecor;
        FooterDecor footerDecor;


        public MessageConstructor(MainConfig cfg)
        {
            mapManager = new ThumbnailManager(cfg);
            this.cfg = cfg;

            var loc = cfg.DiscordConfig.Localization;

            informativeTeamServerDecor = new(loc);
            compactTeamServerDecor = new(loc);
            publicServerDecor = new(loc);
            cvarTableDecor = new(loc);
            footerDecor = new(loc);
        }

        public EmbedBuilder CreateTopBanner()
        {
            var loc = cfg.DiscordConfig.Localization;

            var e = new EmbedBuilder();
            e.ImageUrl = loc.TopBannerIconUrl;
            foreach (var x in loc.TopBannerText)
            {
                e.AddField(x.Key, x.Value);
            }

            return e;
        }

        public async Task<ServerPanel> CreatePanel(ServerInfo server)
        {
            var panel = new ServerPanel();
            var settings = server.Settings;
            var loc = cfg.DiscordConfig.Localization;

            // Map preview
            var topEmbed = new EmbedData();
            panel.MapPreview.Add(topEmbed);

            server.GetCvar("sv_hostname", out string serverName);
            topEmbed.Title = $"{serverName} - `{server.ConnectionInfo}`";

            if (!server.GetCvar("mapname", out string mapName))
                Log.Write($"No map name!", LogCol.Red);

            topEmbed.ImageUrl = await mapManager.GetThumbnail(mapName);

            //sanize player names
            server.Players.ForEach(x => x.Nick.UrTSanetize());

            //order based on score or kills
            server.Players = server.Players.OrderByDescending(x =>
            {
                if (!int.TryParse(x.Score, out int score))
                    score = -1;
                if (!int.TryParse(x.Kills, out int kills))
                    kills = -1;

                return server.HasRconInfo ? kills : score;
            }).ToList();

            if (server.HasRconInfo) //rcon mode
            {
                if (settings.DisplayMode == ServerViewMode.Informative)
                {
                    informativeTeamServerDecor.DecorateView(panel, server);
                }
                else if (settings.DisplayMode == ServerViewMode.Compact)
                {
                    compactTeamServerDecor.DecorateView(panel, server);
                }
                else // error
                {
                    panel.PlayerLists.LastOrDefault()?.AddField(":red_circle: Error", $"Invalid display mode! Current: {settings.DisplayMode}");
                }
            }
            else //non-rcon mode
            {
                publicServerDecor.DecorateView(panel, server);
            }

            cvarTableDecor.DecorateView(panel, server);
            footerDecor.DecorateView(panel, server);

            return panel;
        }

        public EmbedBuilder CreateStatus(string failed, MainConfig cfg)
        {
            var loc = cfg.DiscordConfig.Localization;
            var embed = new EmbedBuilder();

            if (loc.EnableBottomBanner)
            {
                foreach (var x in loc.BottomBannerText)
                {
                    if (x.Key == string.Empty && x.Value == string.Empty)
                        continue;

                    embed.AddField(x.Key.DiscordSanitize(loc), x.Value.DiscordSanitize(loc));
                }

                embed.ImageUrl = loc.BottomBannerIconUrl;

            }
            if (failed != "")
            {
                embed.AddField(loc.MissingServers, failed);
                embed.Color = Color.Red;
            }
            else
                embed.Color = Color.Green;


            CultureInfo culture = null;
            try
            {
                culture = new CultureInfo(loc.Culture);
            }
            catch
            {
                culture = new CultureInfo("en - US");
            }


            TimeZoneInfo timeZone = null;
            try
            {
                timeZone = TimeZoneInfo.FindSystemTimeZoneById(loc.TimeZone);
            }
            catch
            {
                timeZone = TimeZoneInfo.Local;
            }

            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
            var date = now.ToString("dddd, MMMM d, yyyy", culture);
            var time = now.ToString("H:mm:ss", culture);

            embed.Footer = new EmbedFooterBuilder();
            embed.Footer.Text = string.Format(loc.StatusMesage, $"{date} : {time}");
            embed.Footer.IconUrl = loc.SmallIconUrl;

            return embed;
        }
    }
}