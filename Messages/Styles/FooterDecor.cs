﻿using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    /// <summary>
    /// Decorator that inserts a customized footer under all server panels.
    /// </summary>
    public class FooterDecor : IPanelDecorator
    {
        readonly DiscordLocalization localization;

        public FooterDecor(DiscordLocalization localization)
        {
            this.localization = localization;
        }

        public void DecorateView(ServerPanel panel, ServerInfo server)
        {
            var embed = panel.PlayerLists.LastOrDefault();
            if (embed == null)
            {
                embed = new EmbedData();
                panel.PlayerLists.Add(embed);
            }

            var joinLink = string.Format(localization.ClickToJoin, $"<urt://{server.ConnectionInfo}>")
                                 .DiscordSanitize(localization);

            embed.AddField(localization.BlankEmoji, joinLink);

            embed.FooterIconUrl = server.Settings.FooterIconUrl;
            embed.FooterText = server.Settings.FooterText;
        }
    }
}