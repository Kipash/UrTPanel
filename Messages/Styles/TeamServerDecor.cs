﻿using UrtPanel.Models;

namespace UrTPanel.Messages
{
    /// <summary>
    /// Base for Team based decorators. Contains helpers.
    /// </summary>
    public abstract class TeamServerDecor
    {
        /// <summary>
        /// Clamps a number in a string to a fixed string width
        /// Example:raw (100) and max (2) results in 99
        /// </summary>
        protected string ClampStat(string raw, int max)
        {
            uint.TryParse(raw, out var stat);

            if (raw.Length > max)
                return "".PadRight(max, '9');
            else
                return raw;
        }

        /// <summary>
        /// Based on number of teams choose the nick width 
        /// </summary>
        protected int GetNickWidth(ServerInfo server)
        {
            int teams = 0;

            teams += server.SpecTeam.Count != 0 ? 1 : 0;
            teams += server.FreeTeam.Count != 0 ? 1 : 0;
            teams += server.IsTeamMode ? 2 : 0;

            int width = 13;
            if (teams == 1)
                width = 50;
            if (teams == 2)
                width = 22;
            if (teams == 3)
                width = 13;

            return width;
        }
    }
}