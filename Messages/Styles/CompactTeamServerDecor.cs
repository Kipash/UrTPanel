﻿using System.Collections.Generic;
using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    /// <summary>
    /// Decorator for a Team based view - Compact. Goal is to have enough space for everyting. Should be the most visually pleasing.
    /// </summary>
    public class CompactTeamServerDecor : TeamServerDecor, IPanelDecorator
    {
        readonly DiscordLocalization localization;

        public CompactTeamServerDecor(DiscordLocalization localization)
        {
            this.localization = localization;
        }

        public void DecorateView(ServerPanel panel, ServerInfo server)
        {
            server.GetCvar("g_namered", out var redName);
            server.GetCvar("g_nameblue", out var blueName);

            server.GetCvar("redScore", out var redScore);
            server.GetCvar("blueScore", out var blueScore);


            var embed = new EmbedData();

            if (server.IsTeamMode)
            {
                WriteTeam(embed, "Blue", blueName, blueScore, server.BlueTeam, server);
                WriteTeam(embed, "Red", redName, redScore, server.RedTeam, server);
            }
            else
                WriteTeam(embed, "Players", "", "", server.FreeTeam, server);

            if (server.SpecTeam.Count > 0)
                WriteTeam(embed, "Spectator", "", "", server.SpecTeam, server);


            panel.PlayerLists.Add(embed);
        }

        /// <summary>
        /// Writes whole team's scoretable to the embed
        /// </summary>
        void WriteTeam(EmbedData embed, string defaultTeamName, string teamName, string score, List<PlayerInfo> team, ServerInfo server)
        {
            WriteTeam(embed,
                      ComposeHeader(defaultTeamName, teamName, score, server),
                      string.Join("\n", team.Select(pl => ComposePlayer(pl, server))));
        }

        /// <summary>
        /// Writes a provided string to the embed
        /// </summary>
        void WriteTeam(EmbedData embed, string title, string toSend)
        {
            if (toSend.Length == 0)
                toSend = localization.NoPlayers;
            embed.AddField(title.DiscordSanitize(localization), toSend.DiscordSanitize(localization), true);
        }

        /// <summary>
        /// Composes a player line. "{Name} {K} {D} {A}"
        /// </summary>
        string ComposePlayer(PlayerInfo pl, ServerInfo server)
        {
            int width = GetNickWidth(server);

            var playerName = $"{pl.Nick} ({pl.Auth})".ClampString(width);
            var playerNameFormat = "{0,-" + width + "}";
            var botName = $"{"Bot".CenterString(width)}";
            var name = localization.CensureBotNames && pl.IsBot ? botName : string.Format(playerNameFormat, playerName);
            //var nick = $"{pl.Nick.ClampString(width)}";
            var score = $"{ClampStat(pl.Kills, 2),2} {ClampStat(pl.Deaths, 2),2} {ClampStat(pl.Assists, 2),2}";

            return $"`{name.PadRight(width, ' ')} {score}`";
        }

        /// <summary>
        /// Composes a team line. "{Name} ({Score})"
        /// </summary>
        protected string ComposeHeader(string defVal, string name, string score, ServerInfo server)
        {
            var finalName = (!string.IsNullOrEmpty(name) ? name : defVal) + (!string.IsNullOrEmpty(score) ? $" ({score})" : "");
            int width = GetNickWidth(server);
            return $"`{$"{finalName.ClampString(width)}".PadRight(width, ' ')}  K  D  A`";
        }
    }
}