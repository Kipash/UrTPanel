﻿using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    public interface IPanelDecorator
    {
        void DecorateView(ServerPanel panel, ServerInfo server);
    }
}
