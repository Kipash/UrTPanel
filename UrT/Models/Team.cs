﻿namespace UrtPanel.Models
{
    public enum Team
    {
        none = 0,
        Red,
        Blue,
        Free,
        Spectator
    }
}
