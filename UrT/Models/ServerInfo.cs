﻿using System.Collections.Generic;
using UrtPanel.Config;
using UrtPanel.Messages;

namespace UrtPanel.Models
{
    /// <summary>
    /// Game server runtime information
    /// </summary>
    public class ServerInfo
    {
        /// <summary>
        /// Successfully gather player info based on rcon access
        /// </summary>
        public bool HasRconInfo;

        /// <summary>
        /// Incorrect rcon password
        /// </summary>
        public bool InvalidRconPassword;

        /// <summary>
        /// Base settings of the given server. Defined in the config.
        /// </summary>
        public UrTServerCofig Settings;

        /// <summary>
        /// Console variables. Recommended to use "GetCvar" utility function.
        /// </summary>
        public Dictionary<string, string> Cvars = new Dictionary<string, string>();

        /// <summary>
        /// Wheter to report absence
        /// </summary>
        public bool Important;

        /// <summary>
        /// IPv4
        /// </summary>
        public string IP;

        /// <summary>
        /// 
        /// </summary>
        public int Port;

        /// <summary>
        /// Password for rcon access
        /// </summary>
        public string RconPassword;

        /// <summary>
        /// IP:PORT
        /// </summary>
        public string ConnectionInfo => $"{IP}:{Port}";

        /// <summary>
        /// Total player count
        /// </summary>
        public int PlayerCount => Players.Count;

        /// <summary>
        /// Fetched server status
        /// </summary>
        public string StatusData = "";

        /// <summary>
        /// Fetched player stats
        /// </summary>
        public string RconData = "";

        /// <summary>
        /// All players
        /// </summary>
        public List<PlayerInfo> Players = new List<PlayerInfo>();

        /// <summary>
        /// Players in red team
        /// </summary>
        public List<PlayerInfo> RedTeam = new List<PlayerInfo>();

        /// <summary>
        /// Players in blue team
        /// </summary>
        public List<PlayerInfo> BlueTeam = new List<PlayerInfo>();

        /// <summary>
        /// Players in free team. Used for FFA, JUMP and simmilar, tied to IsTeamMode flag
        /// </summary>
        public List<PlayerInfo> FreeTeam = new List<PlayerInfo>();

        /// <summary>
        /// Players in Spectator team
        /// </summary>
        public List<PlayerInfo> SpecTeam = new List<PlayerInfo>();

        /// <summary>
        /// Is the game mode a team based one?
        /// </summary>
        public bool IsTeamMode;

        /// <summary>
        /// Is there an error?
        /// </summary>
        public bool Error => FailCount != 0;

        /// <summary>
        /// How many loop interations the server reported an error.
        /// </summary>
        public uint FailCount;

        /// <summary>
        /// Gets a Cvar and sanitizes the value
        /// </summary>
        public bool GetCvar(string name, out string value)
        {
            bool isValid = Cvars.TryGetValue(name, out value);
            value = value.UrTSanetize();
            return isValid;
        }

        public ServerInfo(UrTServerCofig cfg)
        {
            IP = cfg.IP;
            Port = cfg.Port;
            RconPassword = cfg.RconPassword;
            Important = cfg.Important;
            Settings = cfg;
        }
    }
}
