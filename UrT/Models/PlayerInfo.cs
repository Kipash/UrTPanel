﻿namespace UrtPanel.Models
{
    /// <summary>
    /// Player runtime information
    /// </summary>
    public class PlayerInfo
    {
        /// <summary>
        /// Ingame name
        /// </summary>
        public string Nick;

        /// <summary>
        /// Auth, can be blank
        /// </summary>
        public string Auth;

        /// <summary>
        /// Player kills
        /// </summary>
        public string Kills;

        /// <summary>
        /// Player deaths
        /// </summary>
        public string Deaths;

        /// <summary>
        /// Player assists
        /// </summary>
        public string Assists;

        /// <summary>
        /// Player score which is dependant on the gamemode
        /// </summary>
        public string Score;

        /// <summary>
        /// Team the player is in
        /// </summary>
        public Team Team;

        /// <summary>
        /// Is the user a bot
        /// </summary>
        public bool IsBot;
    }
}
