﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UrtPanel.Config;

namespace UrtPanel.Thumbnails
{
    /// <summary>
    /// Downalods a manifest of valid thumnail names and composes URLs for them
    ///     - Get Manifest
    ///     - Get thumbnail URL based on map name
    /// </summary>
    public class ThumbnailManager
    {
        const string DefaultThumbnail = "ut4_default";
        public string[] MapManifest = new string[0];

        MainConfig cfg;

        public ThumbnailManager(MainConfig cfg)
        {
            this.cfg = cfg;
        }

        /// <summary>
        /// If a thumbnail doesn't exist, it falls back to the default map thumbnail
        /// </summary>
        public async Task<string> GetThumbnail(string mapName)
        {
            if (MapManifest.Length == 0)
                await GetManifest(cfg);

            string url = $"{cfg.UrTConfig.ThumbnailUrl}";
            if (MapManifest.Contains(mapName))
                url += $"{mapName}.jpg";
            else
                url += $"{DefaultThumbnail}.jpg";

            return url;
        }

        /// <summary>
        /// Downloads manifest
        /// </summary>
        async Task GetManifest(MainConfig cfg)
        {
            try
            {
                var url = $"{cfg.UrTConfig.ThumbnailUrl}manifest.json";
                var data = await Download(url);
                var json = Encoding.Default.GetString(data);
                MapManifest = JsonConvert.DeserializeObject<string[]>(json);

                Console.WriteLine($"Map manifest: {MapManifest.Length} maps");
            }
            catch (Exception e)
            {
                Log.Write($"ERROR: \n{e.Message}\n{e.StackTrace}", LogCol.Red);
            }
        }

        /// <summary>
        /// Downloads data. Empty array means error.
        /// </summary>
        async Task<byte[]> Download(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    Console.WriteLine($"Downloading: {url}");
                    var response = await client.GetAsync(url);
                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStreamAsync()}");

                    return await response.Content.ReadAsByteArrayAsync();
                }
                catch (Exception e)
                {
                    Log.Write($"ERROR: \n{e.Message}\n{e.StackTrace}", LogCol.Red);
                    return new byte[0];
                }
            }
        }
    }
}
