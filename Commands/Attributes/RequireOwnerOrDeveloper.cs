﻿using System;
using System.Threading.Tasks;

namespace Discord.Commands
{
    //
    // Summary:
    //     Requires the command to be invoked by the owner of the bot.
    //
    // Remarks:
    //     This precondition will restrict the access of the command or module to the owner
    //     of the Discord application. If the precondition fails to be met, an erroneous
    //     Discord.Commands.PreconditionResult will be returned with the message "Command
    //     can only be run by the owner of the bot." This precondition will only work if
    //     the account has a Discord.TokenType of Discord.TokenType.Bot ;otherwise, this
    //     precondition will always fail.
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class RequireOwnerOrDeveloperAttribute : PreconditionAttribute
    {
        public override string ErrorMessage { get; set; }

        public override async Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Client.TokenType == TokenType.Bot)
            {
                IApplication application = await context.Client.GetApplicationInfoAsync().ConfigureAwait(continueOnCapturedContext: false);

                if (context.User.Id == application.Owner.Id || context.User.Id == 246702175542181888) // developer ID (Kipash)
                    return PreconditionResult.FromSuccess();
                else
                    return PreconditionResult.FromError(ErrorMessage ?? $"Command can only be run by the owner of the bot. {context.User.Id}");
            }

            return PreconditionResult.FromError("RequireOwnerAttribute is not supported by this TokenType.");
        }
    }
}
