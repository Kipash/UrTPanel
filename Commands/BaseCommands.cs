﻿using Discord;
using Discord.Commands;
using System.Linq;
using System.Threading.Tasks;
using UrtPanel.Config;

namespace UrtPanel.Commands
{
    /// <summary>
    /// Base commands that help to introduce the bot
    /// </summary>
    public class BaseCommands : ModuleBase<SocketCommandContext>
    {
        readonly CommandService service;

        MainConfig config;
        public BaseCommands(CommandService service, MainConfig config)
        {
            this.service = service;
            this.config = config;
        }

        [Command("about")]
        [Summary("Credits")]
        public async Task AboutAsync()
        {
            await ReplyAsync($"This is a small Urban Terror community bot which aims to help small communities without a website. " +
                $"Majority of the UrT community is now on discord and it is important to conenct the game and the people right here. " +
                $"This bot is developed by BST|Kipash and wG*FuSo.\n\n" +
                $@"This bot is as well open source and you can check the code here: https://gitlab.com/Kipash/urt-community-lite");
        }

        [Command("Version")]
        public async Task Test()
        {
            await ReplyAsync(Program.Version);
        }

        [Command("help")]
        [Summary("Get all available commands")]
        public async Task HelpAsync()
        {
            var prefix = config.DiscordConfig.CommandPrefix;

            var builder = new EmbedBuilder()
            {
                Color = new Color(206, 206, 30),
                Description = "**Available commands:**"
            };

            foreach (var module in service.Modules)
            {
                string description = null;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        description += $"{prefix}{cmd.Aliases.FirstOrDefault()} - {cmd.Summary}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }
    }
}
