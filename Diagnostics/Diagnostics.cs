﻿using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;

namespace UrTPanel.Diagnostics
{
    /// <summary>
    /// Helper methods to generate debug messages
    /// </summary>
    public class Diagnostics
    {
        public static DiscordMessage ShowConfig()
        {
            DiscordMessage mes = new DiscordMessage();

            //remove private info
            var newCfg = MainConfig.LoadConfigRaw();
            newCfg.DiscordConfig.Token = $"token len: {newCfg.DiscordConfig.Token.Length}";

            foreach (var x in newCfg.UrTConfig.Servers)
            {
                x.RconPassword = string.IsNullOrEmpty(x.RconPassword) ? "no" : "yes";
            }

            var json = MainConfig.SaveConfigRaw(newCfg);
            foreach (var x in json.Split('\n'))
            {
                mes.Add(x);
            }

            return mes;
        }

        public static DiscordMessage ShowServerInfo(ServerInfo[] info)
        {
            DiscordMessage mes = new DiscordMessage();

            foreach (var x in info)
            {
                mes.Add($"$$$$$$$$$$\nServer: {x.ConnectionInfo}\n");
                mes.Add($"Cvars: {string.Join("\n", x.Cvars.Select(x => $"{x.Key}:{x.Value}"))}\n");
                mes.Add($"no status: {x.Error}\n");
                mes.Add($"fail count: {x.FailCount}\n");
                mes.Add($"has rcon info: {x.HasRconInfo}\n");
                mes.Add($"invalid rcon: {x.InvalidRconPassword}\n");
                mes.Add($"player count: {x.PlayerCount}\n");

                foreach (var y in x.StatusData.Split('\n'))
                {
                    mes.Add($"{y}\n");
                }
                foreach (var y in x.RconData.Split('\n'))
                {
                    mes.Add($"{y}\n");
                }
            }

            return mes;
        }
    }
}
